
var new_app = angular.module('myApp',[])

console.log(new_app,":new_app");

new_app.run(function($rootScope){
	$rootScope.user = {
		'email' : 'makrandgurav@yahoo.in'
	};
	$rootScope.name = 'Eddard';
	$rootScope.lname = 'Stark';

});


new_app.controller('firstController',['$scope', function($scope){
	$scope.playing = false;
	$scope.aelement = document.createElement('audio');
	$scope.aelement.src = '/experimental/angular_todo/vote4Delhi.wav';

	$scope.toast_text = "Invalid";
	$scope.show_toast = "false";

	$scope.def_count = 5;
	$scope.play = function() {
		console.log("Playing");
	    $scope.aelement.play();
	    $scope.playing = true;
	};
  	$scope.stop = function() {
  		console.log("stopping");
    	$scope.aelement.pause();
    	$scope.playing = false;
  	};

  	var updateClock = function() {
  		d = Math.floor(Date.now() / 1000);
		var hh = new Date(d*1000)
        $scope.clockHours = hh.getHours();
	    $scope.clockMinutes = hh.getMinutes();
	    $scope.clockSeconds = hh.getSeconds();
  	}

  	var timer = setInterval(function() {
    	$scope.$apply(updateClock);
  	}, 1000);


  	$scope.add = function() {
		$scope.def_count += 1;  		
  	}

  	$scope.sub = function() {
		if($scope.def_count >0) {
			$scope.def_count -= 1;  		
		}
		else {
			$scope.show_toast = "true";
			$scope.toast_text = "Can't Subtract below 0 eh..";
			setTimeout(function() {
				$scope.show_toast="false";
			},2000)
			$scope.def_count = 1;
		}
  	}
}]);


