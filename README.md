The code in this repo is bound to change from time to time. It is adviced that if files from this repo are modified, it should have a start and end change description by the author along with the description of the code added. 

A personal suggestion would be to follow code methodology using the following[Beginners Tutorials](http://www.ng-newsletter.com/posts/beginner2expert-how_to_start.html)
This repo only reflects the code that we need to pass for tutorial purposes

Excellent Angular Js tutorials:

[Angular js tutorials](https://code.angularjs.org/1.2.26/docs/tutorial)

Important Concept in Angular :

**Injector**: Passed as a parameter to functions (Eg $httpd, $routeProvider)

**Service** : Its a singleton object so common across all the controllers. returns an instance of the function to be used across the entire website (Eg session). Services provide a method for us to keep data around for the lifetime of the app and communicate across controllers in a consistent manner.

**Factory**: A new object needs to be created for every controller / factory object. it returns an object and value differs from controller to controller (Eg Billing)

[Gist Example](http://jsfiddle.net/pkozlowski_opensource/PxdSP/14/)

1) When you’re using a **Factory** you create an object, add properties to it, then return that same object. When you pass this factory into your controller, those properties on the object will now be available in that controller through your factory.

2) When you’re using **Service**, AngularJS instantiates it behind the scenes with the ‘new’ keyword. Because of that, you’ll add properties to ‘this’ and the service will return ‘this’. When you pass the service into your controller, those properties on ‘this’ will now be available on that controller through your service.

3) Providers are the only service you can pass into your .config() function. Use a provider when you want to provide module-wide configuration for your service object before making it available.


The **$rootScope** is the top level scope for the rest of our application.
That means that anywhere in the view (i.e. all children elements under the DOM element with the ngApp directive) we can reference variables that are on the $rootScope object.
**Eg. $rootScope.email = 'makrand@yahoo.com' can used as 
<input type = 'text' ng-model = 'email'></input>**


Services that Angular provides : 

**$scope** : Glue between views and controllers. Scope is specific controller related. $rootScope is model related

**$http** : Gives us many different ways to call AJAX services





PS . This is also a good reference though i have never cloned it but some of the examples point here

[GITHUB Reference beginner examples](https://github.com/auser/ng-newsletter-beginner-series/branches)